root@ansible-master:~# useradd devops1
root@ansible-master:~# 
root@ansible-master:~# grep devops1 /etc/passwd /etc/shadow /etc/group /etc/gshadow
/etc/passwd:devops1:x:1001:1001::/home/devops1:/bin/sh
/etc/shadow:devops1:!:18723:0:99999:7:::
/etc/group:devops1:x:1001:
/etc/gshadow:devops1:!::
root@ansible-master:~# 
root@ansible-master:~# 
root@ansible-master:~# 
root@ansible-master:~# adduser azuredevops
Adding user `azuredevops' ...
Adding new group `azuredevops' (1002) ...
Adding new user `azuredevops' (1002) with group `azuredevops' ...
Creating home directory `/home/azuredevops' ...
Copying files from `/etc/skel' ...
Enter new UNIX password: 
Retype new UNIX password: 
passwd: password updated successfully
Changing the user information for azuredevops
Enter the new value, or press ENTER for the default
	Full Name []: Azure DevOps
	Room Number []: 103
	Work Phone []: 00102938389
	Home Phone []: 494948489
	Other []: 4994949
Is the information correct? [Y/n] y
root@ansible-master:~# 
root@ansible-master:~# grep azuredevops /etc/passwd /etc/shadow /etc/group /etc/gshadow
/etc/passwd:azuredevops:x:1002:1002:Azure DevOps,103,00102938389,494948489,4994949:/home/azuredevops:/bin/bash
/etc/shadow:azuredevops:$6$2zRwywZc$GtVdU5NFABpyYr57Vxox6VY2o0CfKoXi4WSQKBDUbloE2icYeKiy8wN6D5HGB/LzHPKr8OA/.3NdY0KkoiM7k0:18723:0:99999:7:::
/etc/group:azuredevops:x:1002:
/etc/gshadow:azuredevops:!::
root@ansible-master:~# 


root@ansible-master:~# grep devops1 /etc/passwd /etc/shadow /etc/group /etc/gshadow
/etc/passwd:devops1:x:1001:1001::/home/devops1:/bin/sh
/etc/shadow:devops1:$6$NWfk8GjB$R6O1MgQzhIjZDpsOIWCHKoEQFOLE7Rfq881BJaM0u5bHL4xaOWdkkLrP11bqbNaZC5iXhUlqEzBKmA.ML4vEQ.:18723:0:99999:7:::
/etc/group:devops1:x:1001:
/etc/gshadow:devops1:!::
root@ansible-master:~# 
root@ansible-master:~# userdel -r devops1
userdel: devops1 mail spool (/var/mail/devops1) not found
userdel: devops1 home directory (/home/devops1) not found
root@ansible-master:~# 
root@ansible-master:~# grep devops1 /etc/passwd /etc/shadow /etc/group /etc/gshadow
root@ansible-master:~# 



root@ansible-master:~# # System Users : 0 - 999

root@ansible-master:~# # Normal Users : 1000 - 65532
