DAY-3 : https://20210331awsdevops.s3.amazonaws.com/DAY-3_20210405/zoom_0.mp4

Agenda :

Basic & Advanced Commands 

File & Director Management 
    - touch <filename>
    - Editors : vi and vim 
    - mkdir 
    - rmdir 
    - rm 
    - mkdir -p abc/123/a1/b1/c1 

Permissions :

Type of a File Or Directory 
    Owner   Group   Others
-   rw-     rw-     r-- 1 ubuntu ubuntu  376 Apr  5 16:11 devops.txt
d   rwx     rwx     r-x 2 ubuntu ubuntu 4096 Apr  5 16:14 a1

r = read 
w = write 
- = no permissions 
x = executable (Folder / Directory # cd abc/ ) | demo.sh or report.py 

Absolute  mode :

Numeric values for the read, write and execute permissions:
read 4
write 2
execute 1

Symbolic Mode:  
# chmod u=rwx   # chmod g=rwx    
# chmod o=rwx   # chmod u-r   
# chmod g-w     # chmod o-x  
# chmod u+rwx   #chmod o+r 


User Management

Group Management



Q & A 
