DAY-8 : https://20210331awsdevops.s3.amazonaws.com/DAY-8_20210412/zoom_0.mp4

Agenda :

SCM / VCS :
https://gitlab.com/cloudbinary/codewithck

DevOps CI/CD Pipeline tools :

Continuous Integration : Jenkins

Continuous Static Code Quality : SonarQube

Continuous Binary CodeRepository : Jfrog 

Application Server : Apache Tomcat 

Continuous Monitoring : Nagios 


Web Application : 
    - Server Coding : Java 
    - Client Coding : HTML, CSS, JS 
    - Database      : RDBMS - MySQL 
    
Build Tool : Maven 

Unit Testing : JUnit 

Integration Tests / Code Coverage : 
Jacoco  / Surefire 

Go to Jenkins >> Manage Jenkins >> Global Tool Configuration >> Java & MAVEN
# echo $JAVA_HOME
/usr/lib/jvm/java-8-oracle

# echo $MAVEN_HOME
/usr/share/maven/

Go to Jenkins >> Manage Jenkins >> Manage Plugins >> Under Available add below mentioned plugins:

Jenkins Plugins :
1. Conditional Buildstep
2. Deploy to container 
3. Environment Injector
4. Git Parameter 
5. GitHub Branch Source
6. Maven Integration
7. Maven Invoker
8. Publish Over SSH

Create Jobs in Jenkins :

A Build Lifecycle is Made Up of Phases
Each of these build lifecycles is defined by a different list of build phases, wherein a build phase represents a stage in the lifecycle.

For example, the default lifecycle comprises of the following phases (for a complete list of the lifecycle phases, refer to the Lifecycle Reference):

validate - validate the project is correct and all necessary information is available
compile - compile the source code of the project
test - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
package - take the compiled code and package it in its distributable format, such as a JAR.
verify - run any checks on results of integration tests to ensure quality criteria are met
install - install the package into the local repository, for use as a dependency in other projects locally
deploy - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.

https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html



