#!/bin/bash

# Update the Repository on Ubuntu 18.04
sudo apt-get update 

# Install required utility softwares 
# sudo apt-get install vim curl elinks unzip wget tree git -y

# Install Maven 
sudo apt-get install maven -y

# Backup the Environment File
sudo cp -pvr /etc/environment "/etc/environment_$(date +%F_%R)"

# Create Environment Variables 
echo "MAVEN_HOME=/usr/share/maven" >> /etc/environment

# Compile the Configuration 
source /etc/environment