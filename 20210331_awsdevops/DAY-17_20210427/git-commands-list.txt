
Git for All Platforms
htp://git-scm.com

# STEP-1 : SETUP 

Configuring user information used across all local repositories

set a name that is identifiable for credit when review version history
git config --global user.name “[firstname lastname]”


set an email address that will be associated with each history marker
git config --global user.email “[valid-email]”

SETUP & INIT
Configuring user information, initializing and cloning repositories

initialize an existing directory as a Git repository
# git init

retrieve an entire repository from a hosted location via URL
# git clone [url]


STAGE & SNAPSHOT
Working with snapshots and the Git staging area

