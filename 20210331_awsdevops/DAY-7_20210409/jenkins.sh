#!/bin/bash

# Update the Repository on Ubuntu 18.04
sudo apt update 

# Upgrade Ubuntu Server
sudo apt upgrade -y 

# Install required utility softwares 
sudo apt install git curl wget unzip elinks tree -y 

# Download, Install Java 11
sudo apt-get install openjdk-11-jdk -y

# Backup the Environment File
sudo cp -pvr /etc/environment "/etc/environment_$(date +%F_%R)"

# Create Environment Variables 
echo "JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/" >> /etc/environment

# Compile the Configuration 
sudo source /etc/environment

# Add Jenkins Repository 
sudo wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

# Adding the Jenkins Remote Repository URL in Ubuntu Local Repository Configuration file 
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/' > /etc/apt/sources.list.d/jenkins.list

# Update the Repository on Ubuntu 18.04
sudo apt-get update 

# Download, Install Jenkins 
sudo apt-get install jenkins -y

# Verify the jenkins service 
# sudo systemctl status jenkins.service

# Enable Jenkins Daemon/Service at Boot
# sudo systemctl enable jenkins.service

# Restart the Jenkins Daemon/Service 
# sudo systemctl restart jenkins.service

# Usig Process Status Command 
# ps -aux | grep jenkins 


