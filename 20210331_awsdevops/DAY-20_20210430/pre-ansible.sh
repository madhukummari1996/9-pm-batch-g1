#!/bin/bash

# Update the Local Repository with Online Repository 
sudo apt update 

# Upgrade Installed Softwares based on Online Repository changes
sudo apt upgrade -y

# Install Utility Softwares Across 3 Servers 
sudo apt install vi git -y

# Install Ansible Depenciency Software part of 3 Servers 
sudo apt install python -y 